from flask_cors import cross_origin
from flask import request

def register_user_router(app):
    @app.route("/login", methods=["POST"])
    @cross_origin()
    def login():
        data_input = request.json
        if "username" not in data_input or "password" not in data_input:
            return {"msg":"username or password empty!", "data":None, "status":"bad request"}, 400
        if data_input.get("username",None) == "nhav" and data_input.get("password",None) == "123456":
            return {"msg":"login success", "data":data_input, "status":"ok"}, 200
        else:
            return {"msg":"Auth fail!", "data":None, "status":"bad request"}, 401