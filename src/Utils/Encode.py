import json
import flask
import datetime

class Encoder(flask.json.JSONEncoder):
    def default(self, obj):
        if obj == None:
            return "null"
        if isinstance(obj, datetime.datetime):
            return str(obj)
        if isinstance(obj, datetime.date):
            return str(obj)
        return super(Encoder, self).default(obj)