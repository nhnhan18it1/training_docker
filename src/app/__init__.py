from flask import Flask
from flask_cors import CORS
from flask_compress import Compress
from router.RouterUser import register_user_router

app = Flask(__name__)
CORS(app)
Compress(app)
register_user_router(app)
