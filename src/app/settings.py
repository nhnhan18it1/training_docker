import os

FLASK_HOST = os.getenv("FLASK_HOST","0.0.0.0")
FLASK_PORT = int(os.getenv("FLASK_PORT",5000))